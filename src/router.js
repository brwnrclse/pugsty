import dispatcher from 'httpdispatcher';
import fs from 'fs';

import {getMime, getFile} from './util.js';

const router = (routeReq, routeRes, routeOpts) => {
  dispatcher.beforeFilter(/\//, (req, res, chain) => {
    res.writeHead(routeOpts.statusCodes.good,
      {'Content-Type': getMime(req.url, routeOpts.mimes)});
    chain.next(req, res);
  });

  dispatcher.onGet(`/`, (req, res) => {
    res.end(getFile(`index.html`, routeOpts.path, routeOpts.regex));
  });

  dispatcher.onGet(`/domtastic/domtastic.js`, (req, res) => {
    res.end(fs.readFileSync(`${__dirname}/assets/domtastic.js`));
  });

  dispatcher.onGet(`/pugsty/client.js`, (req, res) => {
    res.end(fs.readFileSync(`${__dirname}/assets/client.js`));
  });

  dispatcher.onGet(routeOpts.regex.urlRegex, (req, res) => {
    res.end(getFile(req.url, routeOpts.path, routeOpts.regex));
  });

  dispatcher.onError((req, res) => {
    res.writeHead(routeOpts.statusCodes.bad, {'Content-Type': `text/html`});
    res.end(`<h1>Looks like you want something that's not in your pugsty...yet</h1>`);
  });

  dispatcher.dispatch(routeReq, routeRes);
};

export default router;
