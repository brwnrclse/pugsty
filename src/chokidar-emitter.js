import chokidar from 'chokidar';
import socketIO from 'socket.io';

export default (server, path, chokidarOpts) => {
  const
    sio = socketIO(server),
    sockets = [],
    watcher = chokidar.watch(path, chokidarOpts);

  watcher.on(`change`, (changePath) => {
    sockets.map((c) => {
      c.emit(`change`, `${changePath} has changed`);
    });
  });

  sio.on(`connection`, (socket) => {
    const socketIndex = sockets.push(socket);

    socket.emit(`connected`, `Client connected`);
    socket.on(`disconnect`, () => {
      sockets.splice(socketIndex - 1, 1);
    });
  });
};
