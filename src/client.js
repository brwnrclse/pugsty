'use strict';
const socket = io();

socket.on(`connected`, () => {
  console.log(`pugsty, socket connected`);
});

socket.on(`change`, (data) => {
  console.log(data);
});
