#!/usr/bin/env node
import http from 'http';

import chokidarEmitter from './chokidar-emitter.js';
import router from './router.js';

const
  opts = {
    mimes: {
      '/': `text/html`,
      'html': `text/html`,
      'css': `text/css`,
      'js': `application/javascript`
    },
    path: `${process.argv.slice(2)[0]}`,
    port: 2368,
    regex: {
      jsRegex: /\.js$/,
      pugRegx: /\.html$/,
      stylRegx: /\.css$/,
      urlRegex: /\.(css|html)$/,
      ignore: [
        /[\/\\]\./,
        `node_modules`,
        `*.(mp3|mp4|png|jpg|jpeg|svg|gif|html|css)`
      ]
    },
    statusCodes: {
      bad: 404,
      good: 200
    }
  },
  server = http.createServer((req, res) => {
    router(req, res, opts);
  });

chokidarEmitter(server, opts.path, {
  ignore: opts.regex.ignore,
  ignoreInitial: true,
  persistent: true
});

server.listen(opts.port, () => {
  console.log(`Check your Pugsty at localhost:2368`);
});
