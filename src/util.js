import cheerio from 'cheerio';
import fs from 'fs';
import path from 'path';
import pug from 'jade';
import stylus from 'stylus';

const
  getExt = (filePath) => {
    return path.extname(filePath).split(`.`)[1];
  },
  getMime = (url, mimes) => {
    return mimes[getExt(url)];
  },
  inject = (subject) => {
    const
      $ = cheerio.load(subject),
      body = $(`body`),
      scripts = [
        `<script src="/domtastic/domtastic.js"></script>`,
        `<script src="/pugsty/client.js"></script>`,
        `<script src="/socket.io/socket.io.js"></script>`
      ];

    scripts.map((script) => {
      body.append(`${script}`);
    });

    return $.html();
  },
  resolveExt = (url, isPug, {pugRegx, stylRegx}) => {
    return `${isPug ?
        url.replace(pugRegx, `.jade`) : url.replace(stylRegx, `.styl`)}`;
  },
  getFile = (url, uri, regx) => {
    let file = ``;
    const
      isPug = url.match(regx.pugRegx),
      filePath = `${uri}/${resolveExt(url, isPug, regx)}`;

    try {
      file = fs.readFileSync(filePath);
    } catch (e) {
      throw new Error(e);
    } finally {
      return isPug ? inject(pug.render(file)) : stylus(file).render();
    }
  };

export {
  getMime,
  getFile
};
