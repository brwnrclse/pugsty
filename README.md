# Pugsty

What a pugsty! Quickly develop prototypes and wireframes with hot-reloading, pug(jade) and stylus.

### How to use

``` bash
# Go to the project folder
mkdir proj
cd proj
# Make your index file
touch index.jade
# Run pugsty!
pugsty

```

Then open your browser and start writing your pugsty!

Checkout the source [here](https://bitbucket.org/brwnrclse/pugsty), submit any issues, and help improve the pugsty for all!

#### Inspiration
* [jade-server](https://github.com/ded/jade-server/blob/master/jade-server.js)
* [jade-dev](https://github.com/ZombieHippie/jade-dev/blob/master/jade-dev.js)
